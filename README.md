Apprenticeship in France
========================

Application that retrieves and displays apprenticeship training and job opportunities.

All the application is in french.

The application is divided into three parts:
- a `frontend` with [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript) and the [React](https://reactjs.org/) framework
- a `backend` with [Python](https://www.python.org/) and the [Bottle](https://bottlepy.org/docs/dev/) framework
- a `database` with [Redis](https://redis.io/)

## Docker

If you want to run the application without installing it use [docker](https://www.docker.com). The command `docker-compose up` will create the following containers:

| Container                 | Address                                        |
| ------------------------- | ---------------------------------------------- |
| `apprenticeship_backend`  | [http://localhost:8080](http://localhost:8080) |
| `apprenticeship_frontend` | [http://localhost:3000](http://localhost:3000) |
| `apprenticeship_redis`    | [http://localhost:6379](http://localhost:6379) |

To populate the database you can run the following command:

```
docker exec apprenticeship_backend populate-database
```

## Backend

**Note**: Please consider all the following commands in the `backend` folder.

### Installation

To install the backend environment, run:

```
pip install .
```

### Commands

To populate the [redis](https://redis.io) database, run:

```
populate-database
```

To start the [bottle](http://bottlepy.org) local server, run:

```
python -m apprenticeship.api
```

### Tests

You can run the [pytest](https://docs.pytest.org) tests with:

```
pip install -r test-requirements.txt
pytest
```

## Frontend

**Note**: Please consider all the following commands in the `frontend` folder.

### Installation

To install the frontend environment, run:

```
npm install
```

### Commands

To start the [react](https://reactjs.org/) local server, run:

```
npm run start
```

## Linting

To run the linting tests with [pre-commit](https://pre-commit.com), run:

```
pip install pre-commit
pre-commit run -a
```
