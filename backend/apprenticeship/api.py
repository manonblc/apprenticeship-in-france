import json

from apprenticeship.cors import EnableCors
from apprenticeship.redis_client import REDIS_CLIENT
from bottle import Bottle
from bottle import response
from bottle import run

app = Bottle()

app.install(EnableCors())


def json_error(message, status=404):
    response.status = status
    response.content_type = 'application/json'
    return json.dumps(message)


@app.get('/api/families')
def get_families():
    return REDIS_CLIENT.get_famillies()


@app.get('/api/<family>/fields')
def get_fields(family):
    if not REDIS_CLIENT.family_exists(family):
        return json_error('Family not found')
    return REDIS_CLIENT.get_fields(family)


@app.get('/api/<family>/<field>/professions')
def get_professions(family, field):
    if not REDIS_CLIENT.family_exists(family):
        return json_error('Family not found')
    if not REDIS_CLIENT.field_exists(family, field):
        return json_error('Field not found')
    return REDIS_CLIENT.get_professions(family, field)


if __name__ == '__main__':
    run(app, host='0.0.0.0', port=8080)
