#!/usr/bin/env python3
import argparse
import logging
import os
from tempfile import gettempdir

import requests
import validators
import xlrd
from apprenticeship.redis_client import REDIS_CLIENT
from requests.exceptions import HTTPError

logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')
logger = logging.getLogger(__name__)

XLSX_URL = 'https://pole-emploi.org/files/live/sites/peorg/files/documents/Statistiques-et-analyses/Open-data/ROME/ROME_ArboPrincipale.xlsx'
FIELDNAMES = ['family', 'field', 'profession', 'description', 'code']


def parse_args():
    parser = argparse.ArgumentParser(
        description='Populate redis database from a xlsx url.',
    )
    parser.add_argument(
        '--xlsx-url', type=str, help='The xlsx file url to download', default=XLSX_URL,
    )
    parser.add_argument(
        '--drop', action='store_true', help='Drop the database before saving results',
    )
    parser.add_argument(
        '-v', '--verbose', action='store_true', help='Active verbose mode',
    )
    return parser.parse_args()


def check_args(args):
    xlsx_url = args.xlsx_url
    assert validators.url(
        xlsx_url,
    ), f'The xlsx url {xlsx_url} is not a correct url'


def download_xlsx(url):
    tmp_dir = gettempdir()
    # Download the xlsx file
    response = requests.get(url, allow_redirects=True)
    response.raise_for_status()
    xlsx_path = os.path.join(tmp_dir, 'rome_code.xlsx')
    open(xlsx_path, 'wb').write(response.content)
    return xlsx_path


def check_xlsx(path):
    workbook = xlrd.open_workbook(path)
    sheet_names = [
        name for name in workbook.sheet_names()
        if 'Arbo Principale' in name
    ]
    assert (
        len(sheet_names) == 1
    ), f'Found {len(sheet_names)} sheet(s) for the xlsx file {path}'
    return workbook.sheet_by_name(sheet_names.pop())


def read_xlsx(sheet):
    results = []

    # Skip the first line that contains the header
    rows = sheet.get_rows()
    next(rows)
    for row in rows:
        # Convert cell values to a dict
        cell = {
            key: str(row[i].value).strip()
            for i, key in enumerate(FIELDNAMES)
        }
        if cell['code']:
            continue
        if not cell['family'] or (not cell['field'] and cell['profession']):
            logger.warning(
                f'Invalid rome code format for "{cell["description"]}"',
            )
            continue
        results.append({
            'family': cell['family'],
            'field': cell['field'],
            'profession': cell['profession'],
            'description': cell['description'],
        })
    return results


def save(results):
    for result in results:
        if not result['profession'] and not result['field']:
            REDIS_CLIENT.add_family(result['family'], result['description'])
        elif not result['profession']:
            REDIS_CLIENT.add_field(
                result['family'], result['field'], result['description'],
            )
        else:
            REDIS_CLIENT.add_profession(
                result['family'], result['field'], result['profession'], result['description'],
            )


def main():
    args = parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    # Check arguments
    try:
        check_args(args)
    except AssertionError as e:
        logger.error(e)
        return

    # Download xslx file
    logger.debug(f'Downloading xlsx file from {args.xlsx_url}')
    try:
        xlsx_path = download_xlsx(args.xlsx_url)
    except HTTPError as e:
        logger.error(e)
        return
    logger.debug(f'Xlsx file downloaded')

    # Format the xslx file to a list of dict
    logger.debug(f'Parsing the xlsx file {xlsx_path}')
    sheet = check_xlsx(xlsx_path)
    results = read_xlsx(sheet)
    logger.debug(f'File {xlsx_path} parsed')

    # Drop database
    if args.drop:
        logger.debug('Dropping old database')
        REDIS_CLIENT.flush()
        logger.debug('Old database dropped')

    # Save results in the database
    logger.debug('Saving results in the database')
    save(results)
    logger.debug('Results saved')

    REDIS_CLIENT.display_statistiques()


if __name__ == '__main__':
    main()
