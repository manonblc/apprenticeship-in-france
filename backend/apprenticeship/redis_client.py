import logging
import os

from redis import from_url

logger = logging.getLogger(__name__)

DEFAULT_REDIS_URL = 'redis://localhost:6379/0'


class RedisClient:
    def __init__(self):
        redis_url = os.environ.get('REDIS_URL') if os.environ.get(
            'REDIS_URL',
        ) else DEFAULT_REDIS_URL
        self.connection = from_url(redis_url, decode_responses=True)

    def flush(self):
        self.connection.flushdb()

    # Adding functions
    def add_family(self, family, description):
        self.connection.hset('code:family', family, description)

    def add_field(self, family, field, description):
        self.connection.hset(f'code:{family}:field', field, description)

    def add_profession(self, family, field, profession, description):
        self.connection.hset(
            f'code:{family}:{field}:profession', profession, description,
        )

    # Counting functions
    def nb_family(self):
        return self.connection.hlen('code:family')

    def nb_field(self, family):
        return self.connection.hlen(f'code:{family}:field')

    def nb_profession(self, family):
        return sum([self.connection.hlen(f'code:{family}:{field}:profession') for field in self.connection.hkeys(f'code:{family}:field')])

    # Getting functions
    def get_famillies(self):
        return self.connection.hgetall('code:family')

    def get_fields(self, family):
        return self.connection.hgetall(f'code:{family}:field')

    def get_professions(self, family, field):
        return self.connection.hgetall(f'code:{family}:{field}:profession')

    # Existence functions
    def family_exists(self, family):
        return self.connection.hexists('code:family', family)

    def field_exists(self, family, field):
        return self.connection.hexists(f'code:{family}:field', field)

    def family_keys(self):
        return [key for key in self.connection.hkeys('code:family')]

    def display_statistiques(self):
        logger.info(f'Found {self.nb_family()} families')
        for family in self.family_keys():
            logger.info(
                f'Found {self.nb_field(family)} fields and {self.nb_profession(family)} professions for the family {family}',
            )


REDIS_CLIENT = RedisClient()
