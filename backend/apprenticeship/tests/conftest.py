import pytest
from apprenticeship.api import app
from apprenticeship.redis_client import REDIS_CLIENT
from fakeredis import FakeStrictRedis
from webtest import TestApp


@pytest.fixture
def mock_app():
    return TestApp(app)


@pytest.fixture
def mock_redis():
    redis_mock = FakeStrictRedis(decode_responses=True)
    REDIS_CLIENT.connection = redis_mock
    return redis_mock
