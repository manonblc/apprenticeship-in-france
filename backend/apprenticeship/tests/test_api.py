def test_get_families_no_results(mock_app, mock_redis):
    assert mock_app.get('/api/families').json == {}


def test_get_families(mock_app, mock_redis):
    # Setup dabatase
    redis_key = 'code:family'
    expected_results = {'A': 'A description', 'B': 'Other family'}
    for key, value in expected_results.items():
        mock_redis.hset(redis_key, key, value)

    assert mock_app.get('/api/families').json == expected_results


def test_get_fields_error(mock_app, mock_redis):
    res = mock_app.get('/api/A/fields', expect_errors=True)
    assert res.status_code == 404
    assert res.json == 'Family not found'


def test_get_fields(mock_app, mock_redis):
    # Setup dabatase
    # Add a family
    family = 'A'
    redis_key = 'code:family'
    mock_redis.hset(redis_key, family, 'value')
    # Add fields
    redis_key = f'code:{family}:field'
    expected_results = {'01': 'A description', '02': 'Other field'}
    for key, value in expected_results.items():
        mock_redis.hset(redis_key, key, value)
    mock_redis.hset('code:X:field', 'xx', 'Wrong family')

    assert mock_app.get(f'/api/{family}/fields').json == expected_results


def test_get_professions_errors(mock_app, mock_redis):
    res = mock_app.get('/api/A/42/professions', expect_errors=True)
    assert res.status_code == 404
    assert res.json == 'Family not found'

    # Add a family
    family = 'A'
    redis_key = 'code:family'
    mock_redis.hset(redis_key, family, 'Family value')
    res = mock_app.get(f'/api/{family}/42/professions', expect_errors=True)
    assert res.status_code == 404
    assert res.json == 'Field not found'


def test_get_professions(mock_app, mock_redis):
    # Setup dabatase
    # Add a family
    family = 'A'
    redis_key = 'code:family'
    mock_redis.hset(redis_key, family, 'Family value')
    # Add a field
    field = '42'
    redis_key = f'code:{family}:field'
    mock_redis.hset(redis_key, field, 'Fiel value')
    # Add professions
    redis_key = f'code:{family}:{field}:profession'
    expected_results = {'01': 'A description', '02': 'Other profession'}
    for key, value in expected_results.items():
        mock_redis.hset(redis_key, key, value)
    mock_redis.hset('code:A:01:profession', 'xx', 'Wrong field')
    mock_redis.hset('code:X:42:profession', 'xx', 'Wrong family')

    assert mock_app.get(
        f'/api/{family}/{field}/professions',
    ).json == expected_results
