from apprenticeship.cors import EnableCors
from bottle import request
from bottle import response


def some_function(text):
    return f'Text: {text}'


def test_cors_option(monkeypatch):
    monkeypatch.setattr(
        'apprenticeship.cors.request.environ', {
            'REQUEST_METHOD': 'OPTIONS',
        },
    )
    cors_function = EnableCors().apply(some_function, None)
    assert not cors_function(text='test')
    assert response['Access-Control-Allow-Origin'] == '*'


def test_cors():
    cors_function = EnableCors().apply(some_function, None)
    assert cors_function(text='test') == 'Text: test'
    assert response['Access-Control-Allow-Origin'] == '*'
