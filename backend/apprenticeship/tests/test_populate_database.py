import logging
import os
import re
from argparse import Namespace
from pathlib import Path
from tempfile import gettempdir

import pytest
import xlrd
from apprenticeship.populate_database import check_args
from apprenticeship.populate_database import check_xlsx
from apprenticeship.populate_database import download_xlsx
from apprenticeship.populate_database import main
from apprenticeship.populate_database import read_xlsx
from apprenticeship.populate_database import save
from apprenticeship.populate_database import XLSX_URL
from apprenticeship.redis_client import REDIS_CLIENT
from apprenticeship.tests import SAMPLES
from requests.exceptions import HTTPError


def test_check_args():
    url = 'k/test'
    args = Namespace(xlsx_url=url)
    with pytest.raises(AssertionError, match=f'The xlsx url {url} is not a correct url'):
        check_args(args)


def test_download_xslx(requests_mock):
    url = 'http://test.com'
    sample_path = SAMPLES / 'rome_code.xlsx'
    requests_mock.get(url, content=open(sample_path, 'rb').read())
    xlsx_path = download_xlsx(url)
    assert xlsx_path == os.path.join(gettempdir(), 'rome_code.xlsx')
    assert Path(xlsx_path).exists()
    assert open(xlsx_path, 'rb').read() == open(sample_path, 'rb').read()


def test_download_xslx_redirect(requests_mock):
    url = 'http://redirect'
    new_url = 'http://test.com'
    sample_path = SAMPLES / 'rome_code.xlsx'
    requests_mock.get(url, status_code=301, headers={'Location': new_url})
    requests_mock.get(new_url, content=open(sample_path, 'rb').read())
    xlsx_path = download_xlsx(url)
    assert xlsx_path == os.path.join(gettempdir(), 'rome_code.xlsx')
    assert Path(xlsx_path).exists()
    assert open(xlsx_path, 'rb').read() == open(sample_path, 'rb').read()


def test_download_xlsx_error(requests_mock):
    url = 'http://test.com/'
    requests_mock.get(url, status_code=400)
    with pytest.raises(HTTPError, match=f'400 Client Error: None for url: {url}'):
        download_xlsx(url)


def test_check_xlsx():
    sheet = check_xlsx(SAMPLES / 'rome_code.xlsx')
    assert sheet.name == 'Arbo Principale <date>'


def test_check_xlsx_error():
    path = SAMPLES / 'rome_code_error.xlsx'
    with pytest.raises(AssertionError, match=re.escape(f'Found 0 sheet(s) for the xlsx file {path}')):
        check_xlsx(path)


def test_read_xlsx():
    workbook = xlrd.open_workbook(SAMPLES / 'rome_code.xlsx')
    sheet = workbook.sheet_by_name('Arbo Principale <date>')
    results = read_xlsx(sheet)
    assert results == [
        {
            'description': 'Agriculture et Pêche, Espaces naturels et Espaces verts, Soins aux animaux',
            'family': 'A',
            'field': '',
            'profession': '',
        },
        {
            'description': 'Engins agricoles et forestiers',
            'family': 'A',
            'field': '11',
            'profession': '',
        },
        {
            'description': "Conduite d'engins agricoles et forestiers",
            'family': 'A',
            'field': '11',
            'profession': '01',
        },
        {
            'description': 'Espaces naturels et espaces verts',
            'family': 'A',
            'field': '12',
            'profession': '',
        },
        {
            'description': 'Bûcheronnage et élagage',
            'family': 'A',
            'field': '12',
            'profession': '01',
        },
        {
            'description': 'Entretien des espaces naturels',
            'family': 'A',
            'field': '12',
            'profession': '02',
        },
        {
            'description': "Arts et Façonnage d'ouvrages d'art",
            'family': 'B',
            'field': '',
            'profession': '',
        },
        {
            'description': 'Arts plastiques',
            'family': 'B',
            'field': '11',
            'profession': '',
        },
        {
            'description': 'Décoration',
            'family': 'B',
            'field': '13',
            'profession': '',
        },
        {
            'description': "Décoration d'objets d'art et artisanaux",
            'family': 'B',
            'field': '13',
            'profession': '02',
        },
        {
            'description': 'Banque, Assurance, Immobilier',
            'family': 'C',
            'field': '',
            'profession': '',
        },
        {
            'description': 'Assurance',
            'family': 'C',
            'field': '11',
            'profession': '',
        },
        {
            'description': "Conception - développement produits d'assurances",
            'family': 'C',
            'field': '11',
            'profession': '01',
        },
    ]


def test_save(mock_redis):
    save([
        {
            'description': 'Agriculture et Pêche, Espaces naturels et Espaces verts, Soins aux animaux',
            'family': 'A',
            'field': '',
            'profession': '',
        },
        {
            'description': 'Engins agricoles et forestiers',
            'family': 'A',
            'field': '11',
            'profession': '',
        },
        {
            'description': 'Espaces naturels et espaces verts',
            'family': 'A',
            'field': '12',
            'profession': '',
        },
        {
            'description': 'Bûcheronnage et élagage',
            'family': 'A',
            'field': '12',
            'profession': '01',
        },
        {
            'description': "Arts et Façonnage d'ouvrages d'art",
            'family': 'B',
            'field': '',
            'profession': '',
        },
        {
            'description': 'Arts plastiques',
            'family': 'B',
            'field': '11',
            'profession': '',
        },
        {
            'description': 'Décoration',
            'family': 'B',
            'field': '13',
            'profession': '',
        },
        {
            'description': "Décoration d'objets d'art et artisanaux",
            'family': 'B',
            'field': '13',
            'profession': '02',
        },
        {
            'description': 'Banque, Assurance, Immobilier',
            'family': 'C',
            'field': '',
            'profession': '',
        },
        {
            'description': 'Assurance',
            'family': 'C',
            'field': '11',
            'profession': '',
        },
        {
            'description': "Conception - développement produits d'assurances",
            'family': 'C',
            'field': '11',
            'profession': '01',
        },
    ])

    assert REDIS_CLIENT.nb_family() == 3
    assert REDIS_CLIENT.nb_field('A') == 2
    assert REDIS_CLIENT.nb_profession('A') == 1

    assert REDIS_CLIENT.nb_field('B') == 2
    assert REDIS_CLIENT.nb_profession('B') == 1

    assert REDIS_CLIENT.nb_field('C') == 1
    assert REDIS_CLIENT.nb_profession('C') == 1


def test_run(requests_mock, script_runner, mock_redis):
    sample_path = SAMPLES / 'rome_code.xlsx'
    requests_mock.get(XLSX_URL, content=open(sample_path, 'rb').read())

    family = 'T'
    # Family
    redis_key = 'code:family'
    mock_redis.hset(redis_key, 'T', 'new family')
    # First field with two professions
    field = '42'
    redis_key = f'code:{family}:{field}:profession'
    mock_redis.hset(f'code:{family}:field', field, 'A field')
    mock_redis.hset(redis_key, '01', 'bla')
    mock_redis.hset(redis_key, '02', 'new profession')
    # Second field with one profession
    field = '00'
    redis_key = f'code:{family}:{field}:profession'
    mock_redis.hset(f'code:{family}:field', field, 'other field')
    mock_redis.hset(redis_key, '01', 'other profession')

    ret = script_runner.run('populate-database')
    assert ret.success

    assert REDIS_CLIENT.nb_family() == 4
    assert REDIS_CLIENT.nb_field('A') == 2
    assert REDIS_CLIENT.nb_profession('A') == 3

    assert REDIS_CLIENT.nb_field('B') == 2
    assert REDIS_CLIENT.nb_profession('B') == 1

    assert REDIS_CLIENT.nb_field('C') == 1
    assert REDIS_CLIENT.nb_profession('C') == 1

    assert REDIS_CLIENT.nb_field('T') == 2
    assert REDIS_CLIENT.nb_profession('T') == 3


def test_run_drop(requests_mock, script_runner, mock_redis):
    url = 'http://test.com'
    sample_path = SAMPLES / 'rome_code.xlsx'
    requests_mock.get(url, content=open(sample_path, 'rb').read())

    family = 'T'
    # Family
    redis_key = 'code:family'
    mock_redis.hset(redis_key, 'T', 'new family')
    # First field with two professions
    field = '42'
    redis_key = f'code:{family}:{field}:profession'
    mock_redis.hset(f'code:{family}:field', field, 'A field')
    mock_redis.hset(redis_key, '01', 'bla')
    mock_redis.hset(redis_key, '02', 'new profession')
    # Second field with one profession
    field = '00'
    redis_key = f'code:{family}:{field}:profession'
    mock_redis.hset(f'code:{family}:field', field, 'other field')
    mock_redis.hset(redis_key, '01', 'other profession')

    ret = script_runner.run('populate-database', '--xlsx-url', url, '--drop')
    assert ret.success

    assert REDIS_CLIENT.nb_family() == 3
    assert REDIS_CLIENT.nb_field('A') == 2
    assert REDIS_CLIENT.nb_profession('A') == 3

    assert REDIS_CLIENT.nb_field('B') == 2
    assert REDIS_CLIENT.nb_profession('B') == 1

    assert REDIS_CLIENT.nb_field('C') == 1
    assert REDIS_CLIENT.nb_profession('C') == 1

    assert REDIS_CLIENT.nb_field('T') == 0
    assert REDIS_CLIENT.nb_profession('T') == 0


def test_run_logs(requests_mock, mock_redis, monkeypatch, caplog):
    caplog.set_level(logging.DEBUG)

    url = 'http://test.com'
    sample_path = SAMPLES / 'rome_code.xlsx'
    requests_mock.get(url, content=open(sample_path, 'rb').read())

    def mock_parse_args():
        return Namespace(xlsx_url=url, verbose=True, drop=True)

    monkeypatch.setattr(
        'apprenticeship.populate_database.parse_args', mock_parse_args,
    )

    main()

    assert caplog.record_tuples == [
        (
            'apprenticeship.populate_database',
            logging.DEBUG,
            'Downloading xlsx file from http://test.com',
        ),
        (
            'requests_mock.adapter',
            logging.DEBUG,
            'GET http://test.com/ 200',
        ),
        (
            'apprenticeship.populate_database',
            logging.DEBUG,
            'Xlsx file downloaded',
        ),
        (
            'apprenticeship.populate_database',
            logging.DEBUG,
            'Parsing the xlsx file /tmp/rome_code.xlsx',
        ),
        (
            'apprenticeship.populate_database',
            logging.WARNING,
            'Invalid rome code format for "Wrong family"',
        ),
        (
            'apprenticeship.populate_database',
            logging.WARNING,
            'Invalid rome code format for "Wrong field"',
        ),
        (
            'apprenticeship.populate_database',
            logging.DEBUG,
            'File /tmp/rome_code.xlsx parsed',
        ), (
            'apprenticeship.populate_database',
            logging.DEBUG,
            'Dropping old database',
        ),
        (
            'apprenticeship.populate_database',
            logging.DEBUG,
            'Old database dropped',
        ),

        (
            'apprenticeship.populate_database',
            logging.DEBUG,
            'Saving results in the database',
        ),
        (
            'apprenticeship.populate_database',
            logging.DEBUG,
            'Results saved',
        ),
        (
            'apprenticeship.redis_client',
            logging.INFO,
            'Found 3 families',
        ),
        (
            'apprenticeship.redis_client',
            logging.INFO,
            'Found 2 fields and 3 professions for the family A',
        ),
        (
            'apprenticeship.redis_client',
            logging.INFO,
            'Found 2 fields and 1 professions for the family B',
        ),
        (
            'apprenticeship.redis_client',
            logging.INFO,
            'Found 1 fields and 1 professions for the family C',
        ),
    ]


def test_run_help(script_runner):
    ret = script_runner.run('populate-database', '--help')
    assert ret.success
    assert ret.stdout == '\n'.join([
        'usage: populate-database [-h] [--xlsx-url XLSX_URL] [--drop] [-v]',
        '',
        'Populate redis database from a xlsx url.',
        '',
        'optional arguments:',
        '  -h, --help           show this help message and exit',
        '  --xlsx-url XLSX_URL  The xlsx file url to download',
        '  --drop               Drop the database before saving results',
        '  -v, --verbose        Active verbose mode',
        '',
    ])


@pytest.mark.parametrize(
    'url, error_message',
    [
        ('k/test', 'The xlsx url k/test is not a correct url\n'),
        ('http://test.com', '400 Client Error: None for url: http://test.com/\n'),
    ],

)
def test_run_error(url, error_message, requests_mock, script_runner, mock_redis):
    requests_mock.get(url, status_code=400)
    ret = script_runner.run('populate-database', '--xlsx-url', url)
    assert ret.success
    assert ret.stderr == error_message
    assert REDIS_CLIENT.nb_family() == 0
