import pytest
from apprenticeship.redis_client import REDIS_CLIENT
from apprenticeship.redis_client import RedisClient


@pytest.mark.parametrize(
    'environment, expected_kwargs',
    [
        (
            '',
            {
                'db': 0,
                'host': 'localhost',
                'password': None,
                'port': 6379,
                'username': None,
                'decode_responses': True,
            },
        ),
        (
            'redis://test:1234/1',
            {
                'db': 1,
                'host': 'test',
                'password': None,
                'port': 1234,
                'username': None,
                'decode_responses': True,
            },
        ),
    ],
)
def test_init(environment, expected_kwargs, monkeypatch):
    monkeypatch.setenv('REDIS_URL', environment)
    redis = RedisClient()
    assert redis.connection.connection_pool.connection_kwargs == expected_kwargs


def test_flush(mock_redis):
    redis_key = 'redis_key'
    mock_redis.hset(redis_key, 'key', 'value')
    REDIS_CLIENT.flush()
    assert not mock_redis.keys('*')


def test_add_family(mock_redis):
    redis_key = 'code:family'
    mock_redis.hset(redis_key, 'T', 'bla')
    REDIS_CLIENT.add_family('R', 'new family')
    assert sorted([key for key in mock_redis.hkeys(redis_key)]) == ['R', 'T']
    assert mock_redis.hget(redis_key, 'T') == 'bla'
    assert mock_redis.hget(redis_key, 'R') == 'new family'


def test_add_field(mock_redis):
    family = 'T'
    redis_key = f'code:{family}:field'
    mock_redis.hset(redis_key, '01', 'bla')
    REDIS_CLIENT.add_field(family, '02', 'new field')
    assert sorted([key for key in mock_redis.hkeys(redis_key)]) == ['01', '02']
    assert mock_redis.hget(redis_key, '01') == 'bla'
    assert mock_redis.hget(redis_key, '02') == 'new field'


def test_add_profession(mock_redis):
    family = 'T'
    field = '42'
    redis_key = f'code:{family}:{field}:profession'
    mock_redis.hset(redis_key, '01', 'bla')
    REDIS_CLIENT.add_profession(family, field, '02', 'new profession')
    assert sorted([key for key in mock_redis.hkeys(redis_key)]) == ['01', '02']
    assert mock_redis.hget(redis_key, '01') == 'bla'
    assert mock_redis.hget(redis_key, '02') == 'new profession'


def test_nb_family(mock_redis):
    redis_key = 'code:family'
    mock_redis.hset('wrong:key', 'X', '')
    mock_redis.hset(redis_key, 'T', 'bla')
    mock_redis.hset(redis_key, 'R', 'new family')
    assert REDIS_CLIENT.nb_family() == 2


def test_nb_field(mock_redis):
    family = 'T'
    redis_key = f'code:{family}:field'
    mock_redis.hset(f'wrong:{family}:key', 'X', '')
    mock_redis.hset(redis_key, '01', 'bla')
    mock_redis.hset(redis_key, '02', 'new field')
    mock_redis.hset(redis_key, '03', 'field')
    assert REDIS_CLIENT.nb_field(family) == 3


def test_nb_profession(mock_redis):
    family = 'T'
    # First field
    field = '42'
    redis_key = f'code:{family}:{field}:profession'
    mock_redis.hset(f'code:{family}:field', field, 'A field')
    mock_redis.hset(redis_key, '01', 'bla')
    mock_redis.hset(redis_key, '02', 'new profession')
    # Second field
    field = '00'
    redis_key = f'code:{family}:{field}:profession'
    mock_redis.hset(f'code:{family}:field', field, 'other field')
    mock_redis.hset(redis_key, '01', 'other profession')
    # Wrong key
    field = 'oops'
    redis_key = f'wrong:{family}:{field}:profession'
    mock_redis.hset(f'code:{family}:field', field, 'wrong field')
    mock_redis.hset(redis_key, 'X', '')
    assert REDIS_CLIENT.nb_profession(family) == 3


def test_family_keys(mock_redis):
    redis_key = 'code:family'
    mock_redis.hset(redis_key, 'T', 'bla')
    mock_redis.hset(redis_key, 'R', 'new family')
    assert sorted(REDIS_CLIENT.family_keys()) == ['R', 'T']
