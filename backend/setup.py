#!/usr/bin/env python3
import os.path

from setuptools import find_packages
from setuptools import setup


def requirements(path):
    assert os.path.exists(path), f'Missing requirements {path}'
    with open(path) as f:
        return list(map(str.strip, f.read().splitlines()))


with open('VERSION') as f:
    VERSION = f.read()

install_requires = requirements('requirements.txt')

setup(
    name='apprenticeship-backend',
    version=VERSION,
    description='Apprenticeship in France backend',
    author='Manon Blanco',
    author_email='manon.blanco@hotmail.fr',
    project_urls={
        'Source': 'https://gitlab.com/manon-blanco/apprenticeship-in-france',
    },
    entry_points={
        'console_scripts': [
            'populate-database=apprenticeship.populate_database:main',
        ],
    },
    install_requires=install_requires,
    packages=find_packages(),
    licence='MIT',
)
