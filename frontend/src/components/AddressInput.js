import React, { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { IoLocationSharp } from 'react-icons/io5'
import { connect } from 'react-redux'
import { mapDispatchToProps, mapStateToProps } from '../utils'

const AddressInput = (props) => {
  const inputRef = useRef(null)

  /**
   * Mounted functions
   */
  useEffect(async () => {
    // Remove address choices if the user clicks outside a choice
    window.addEventListener('mousedown', handleClick)
  }, [])

  const handleClick = (event) => {
    // Call click event after change event
    setTimeout(async () => {
      const target = event.target
      const tag = target.tagName
      const name = target.name
      if (name !== 'address' || tag !== 'A') await props.getAddresses('')
    }, 1)
  }

  /**
   * Specific function to udpate payload params and call API
   */
  const updateAddress = async (event) => {
    const target = event.target
    const tag = target.tagName
    const value = target.value || target.text || ''

    // The user entered an address
    if (tag === 'INPUT') {
      // Call the api only if the user has finished writing
      setTimeout(async () => {
        if (value === inputRef.current.value) await props.getAddresses(value)
      }, 20)
    } else {
      // The user clicked on an address
      await props.getAddresses('')
    }

    props.setAddress(value)
  }

  return (
    <div className='dropdown is-active'>
      {/* Address input */}
      <div className='dropdown-trigger'>
        <div className='field'>
          <div className='control has-icons-left'>
            <span className='icon is-small'>
              <IoLocationSharp />
            </span>
            <input
              className='input'
              type='text'
              placeholder='Adresse, ville ou code postal'
              name='address'
              ref={inputRef}
              value={props.address}
              onChange={updateAddress}
            />
          </div>
        </div>
      </div>
      {/* Address choices */}
      <div className='dropdown-menu' id='dropdown-menu' role='menu'>
      {props.geo.addresses.length > 0 &&
        <div className='dropdown-content'>
          {props.geo.addresses.map(address => (
            <a
              className='dropdown-item'
              name='address'
              key={address.id}
              onClick={updateAddress}
            >{address.label}</a>
          ))}
        </div>
      }
      </div>
    </div>
  )
}

AddressInput.propTypes = {
  geo: PropTypes.object.isRequired,
  address: PropTypes.string.isRequired,
  getAddresses: PropTypes.func.isRequired,
  setAddress: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(AddressInput)
