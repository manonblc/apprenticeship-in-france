import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { mapStateToProps } from '../utils'
import '../styles/Error.css'

const Error = (props) => {
  return (
    <div className='notification is-danger mb-3 mr-3'>
      {props.utils.error}
    </div>
  )
}

Error.propTypes = {
  utils: PropTypes.object.isRequired
}

export default connect(mapStateToProps)(Error)
