import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { DIPLOMAS } from '../config'
import { useLocation } from 'react-router-dom'
import { mapDispatchToProps, mapStateToProps } from '../utils'
import AddressInput from './AddressInput'
import '../styles/Form.css'

const Form = (props) => {
  const DEFAULT_RADIUS = 30
  const params = new URLSearchParams(useLocation().search)
  const [defaultSearch, setDefaultSearch] = useState(true)
  const [payload, setPayload] = useState({
    address: '',
    departement: '',
    region: '',
    radius: DEFAULT_RADIUS,
    family: '',
    field: '',
    profession: '',
    diploma: ''
  })
  const [place, setPlace] = useState('address')

  /**
   * Custom hook
   */
  const usePrevious = (value) => {
    const ref = useRef()
    useEffect(() => {
      ref.current = value
    })
    return ref.current || payload
  }

  const previousPayload = usePrevious(payload)

  /**
   * Mounted functions
   */
  useEffect(async () => {
    await fetchData()
    parseQueryParams()
  }, [])

  const fetchData = async () => {
    // Geo actions
    await props.getRegions()
    await props.getDepartments()
    // Job actions
    await props.getFamilies()
    const family = params.get('family')
    if (family) {
      await props.getFields(family)
      const field = params.get('field')
      if (field) await props.getProfessions(family, field)
    }
  }

  const parseQueryParams = () => {
    const entries = params.entries()
    for (const [key, value] of entries) {
      if (value) {
        if (key === 'place') setPlace(value)
        else setPayload(payload => ({ ...payload, [key]: value }))
        if (['departement', 'region', 'diploma'].includes(key) || (key === 'radius' && value !== DEFAULT_RADIUS.toString())) setDefaultSearch(false)
      }
    }
  }

  /**
   * Submit the form only if the payload has been modified
   * by the parseParams function and not if the form is updated.
   */
  useEffect(async () => {
    const diff = Object.keys(payload).reduce((diff, key) => {
      if (payload[key] === previousPayload[key]) return diff
      else return { ...diff, [key]: payload[key] }
    }, {})
    for (const key in diff) {
      if (!params.get(key) || payload[key] !== params.get(key)) return
    }
    await submit()
  }, [payload])

  /**
   * Functions to submit form.
   * Cannot use a single function because props.geo.address is not rendered immediately.
   */
  useEffect(async () => {
    if (!Object.keys(props.geo.address).length) return
    const params = {
      romes: romeCode(),
      radius: payload.radius,
      ...props.geo.address
    }
    await props.getJobsAndTraining(params)
    props.setLoading(false)
  }, [props.geo.address])

  const submit = async (event) => {
    if (event) event.preventDefault()
    if (!IsValid()) return
    props.history.replace({
      search: `?${new URLSearchParams(payload).toString()}`
    })
    props.setLoading(true)
    if (onlyTraining()) {
      const params = {
        romes: romeCode(),
        romeDomain: romeDomain(),
        diploma: payload.diploma,
        [place]: payload[place]
      }

      if (payload.profession) delete params.romeDomain
      else delete params.romes

      await props.getTraining(params)
      props.setLoading(false)
    } else {
      await props.getAddress(payload.address)
    }
    props.setSearched(true)
  }

  /**
   * Generic function to update payload params
   */
  const handleChange = (event) => {
    const target = event.target
    setPayload({ ...payload, [target.name]: target.value })
  }

  /**
   * Specific functions to udpate payload params and/or call API
   */
  const setAddress = (value) => {
    setPayload({ ...payload, address: value })
  }

  const updateJob = async (event) => {
    const target = event.target
    const value = target.value
    const name = target.name
    let additional = {}

    // Get fields for this family
    if (name === 'family') {
      await props.getFields(value)
      additional = { field: '', profession: '' }
    }

    // Get professions for this field
    if (name === 'field') {
      if (value) await props.getProfessions(payload.family, value)
      additional = { profession: '' }
    }

    setPayload({ ...payload, ...additional, [name]: value })
  }

  /**
   * Computed functions
   */
  const IsValid = () => {
    return onlyTraining() ? payload[place] && payload.family : payload.address && payload.family && payload.field && payload.profession
  }

  const onlyTraining = () => {
    return !defaultSearch && ['departement', 'region'].includes(place)
  }

  const romeCode = () => {
    return `${romeDomain()}${payload.profession}`
  }

  const romeDomain = () => {
    return `${payload.family}${payload.field}`
  }

  return (
    <form className='column is-narrow border'>
      {/* Type of search */}
      <div className='tabs'>
        <ul>
          <li className={`${defaultSearch ? 'is-active' : ''}`}><a onClick={() => setDefaultSearch(true)}>Recherche rapide</a></li>
          <li className={`${!defaultSearch ? 'is-active' : ''}`}><a onClick={() => setDefaultSearch(false)}>Recherche précise</a></li>
        </ul>
      </div>

      {/* Place field... */}
      <label className='label'>Lieu <span className='has-text-danger'>*</span></label>

      {/* ...for the default search */}
      {defaultSearch &&
        <AddressInput address={payload.address} setAddress={setAddress}/>
      }

      {/* ...for the precise search */}
      {!defaultSearch &&
        <div className='field has-addons'>
          <div className='control'>
            <div className='select'>
              <select
                name='place'
                value={place}
                onChange={(e) => setPlace(e.target.value)}
              >
                <option value='address'>Adresse, ville ou code postal</option>
                <option value='departement'>Département</option>
                <option value='region'>Région</option>
              </select>
            </div>
          </div>
          {/* Address choice */}
          {place === 'address' &&
            <AddressInput address={payload.address} setAddress={setAddress}/>
          }
          {/* Department choice */}
          {place === 'departement' &&
            <div className='control'>
              <div className='select'>
                <select
                  name='departement'
                  value={payload.departement}
                  onChange={handleChange}
                >
                  <option value='' disabled>Selectionner un département</option>
                  {props.geo.departments.map(depertment => (
                    <option value={depertment.code} key={depertment.code}>{depertment.nom}</option>
                  ))}
                </select>
                <p className='help has-text-danger'>Recherche <strong className='has-text-danger'>uniquement</strong> les formations</p>
              </div>
            </div>
          }
          {/* Region choice */}
          {place === 'region' &&
            <div className='control'>
              <div className='select'>
                <select
                  name='region'
                  value={payload.region}
                  onChange={handleChange}
                >
                  <option value='' disabled>Selectionner une région</option>
                  {props.geo.regions.map(region => (
                    <option value={region.code} key={region.code}>{region.nom}</option>
                  ))}
                </select>
                <p className='help has-text-danger'>Recherche <strong className='has-text-danger'>uniquement</strong> les formations</p>
              </div>
            </div>
          }
        </div>
      }

      {/* Radius field */}
      {!defaultSearch &&
        <div className='field'>
          <label className='label'>Dans un rayon de</label>
          <div className='control'>
            <input
              className='input'
              type='number'
              name='radius'
              value={payload.radius}
              onChange={handleChange}
              min='1'
              max='200'
            /><strong className='ml-2'>km</strong>
          </div>
        </div>
      }

      <hr />

      {/* Job family field */}
      <div className='field'>
        <label className='label'>Famille de métier <span className='has-text-danger'>*</span></label>
        <div className='control'>
          <div className='select'>
            <select
              name='family'
              value={payload.family}
              onChange={updateJob}
            >
              <option value='' disabled>Selectionner une famille</option>
              {Object.keys(props.job.families).map(key => (
                <option value={key} key={key}>{props.job.families[key]}</option>
              ))}
            </select>
          </div>
        </div>
      </div>

      {/* Job activity field */}
      <div className='field'>
        <label className='label'>Domaine professionnel {!onlyTraining() && <span className='has-text-danger'>*</span>}</label>
        <div className='control'>
          <div className='select'>
            <select
              name='field'
              value={payload.field}
              onChange={updateJob}
              disabled={!payload.family}
            >
              <option value='' disabled={!onlyTraining()}>---</option>
              {Object.keys(props.job.fields).map(key => (
                <option value={key} key={key}>{props.job.fields[key]}</option>
              ))}
            </select>
          </div>
        </div>
      </div>

      {/* Job field */}
      <div className='field'>
        <label className='label'>Métier {!onlyTraining() && <span className='has-text-danger'>*</span>}</label>
        <div className='control'>
          <div className='select'>
            <select
              name='profession'
              value={payload.profession}
              onChange={updateJob}
              disabled={!payload.field}
            >
              <option value='' disabled={!onlyTraining()}>---</option>
              {Object.keys(props.job.professions).map(key => (
                <option value={key} key={key}>{props.job.professions[key]}</option>
              ))}
            </select>
          </div>
        </div>
      </div>

      {!defaultSearch && <hr />}

      {/* Diploma field */}
      {!defaultSearch &&
        <div className='field'>
          <label className='label'>Niveau de diplôme</label>
          <div className='control'>
            <div className='select'>
              <select
                name='diploma'
                value={payload.diploma}
                onChange={handleChange}
              >
                <option value=''>---</option>
                {Object.keys(DIPLOMAS).map(key => (
                  <option value={key} key={key}>{DIPLOMAS[key]}</option>
                ))}
              </select>
            </div>
          </div>
        </div>
      }

      {/* Submit button */}
      <div className='field is-pulled-right'>
        <button className='button is-link' disabled={!IsValid()} type='submit' onClick={submit}>Valider</button>
      </div>
    </form>
  )
}

Form.propTypes = {
  history: PropTypes.object.isRequired,
  geo: PropTypes.object.isRequired,
  job: PropTypes.object.isRequired,
  getRegions: PropTypes.func.isRequired,
  getDepartments: PropTypes.func.isRequired,
  getAddress: PropTypes.func.isRequired,
  getFamilies: PropTypes.func.isRequired,
  getFields: PropTypes.func.isRequired,
  getProfessions: PropTypes.func.isRequired,
  getJobsAndTraining: PropTypes.func.isRequired,
  getTraining: PropTypes.func.isRequired,
  setSearched: PropTypes.func.isRequired,
  setLoading: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Form))
