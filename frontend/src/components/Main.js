import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { mapStateToProps } from '../utils'
import Form from './Form'
import Results from './Results'
import Error from './Error'
import '../styles/Main.css'

const Main = (props) => {
  const [searched, setSearched] = useState(false)
  const [loading, setLoading] = useState(false)

  return (
    <div className='columns is-multiline'>
      <Form setSearched={setSearched} setLoading={setLoading} />
      {searched && <Results loading={loading} />}
      {props.utils.error && <Error />}
    </div>
  )
}

Main.propTypes = {
  utils: PropTypes.object.isRequired
}

export default connect(mapStateToProps)(Main)
