import React from 'react'
import PropTypes from 'prop-types'
import { IoLocationSharp, IoMailSharp, IoBriefcaseSharp, IoSchoolSharp, IoHomeSharp, IoCallSharp } from 'react-icons/io5'

const Result = (props) => {
  /**
   * Computed function
   */
  const icon = () => {
    switch (props.type) {
      case 'training':
        return <IoSchoolSharp className='mr-4'/>
      case 'company':
        return <IoHomeSharp className='mr-4'/>
      case 'job':
        return <IoBriefcaseSharp className='mr-4'/>
      default:
        return <div className='mr-4'></div>
    }
  }

  return (
    <div className='is-flex'>
      {icon()}
      <div>
        {/* Title */}
        <strong>{props.result.title}</strong>
        <div>{props.result.company.name}</div>

        {/* Address */}
        <div className='is-centered'>
          <IoLocationSharp className='mr-2'/>
          <div>{props.result.place.fullAddress}</div>
        </div>

        {/* Contact */}
        {props.result.contact && props.result.contact.email &&
          <div className='is-centered'>
            <IoMailSharp className='mr-2'/>
            <div>{props.result.contact.email}</div>
          </div>
        }
        {props.result.contact && props.result.contact.phone &&
          <div className='is-centered'>
            <IoCallSharp className='mr-2'/>
            <div>{props.result.contact.phone}</div>
          </div>
        }
        {props.result.url && <a href={props.result.url} target='_blank' rel='noopener noreferrer'>Plus d&apos;info</a>}
      </div>
    </div>
  )
}

Result.propTypes = {
  type: PropTypes.string.isRequired,
  result: PropTypes.object.isRequired
}

export default Result
