import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { IoSchoolSharp, IoBriefcaseSharp, IoHomeSharp } from 'react-icons/io5'
import { mapStateToProps } from '../utils'
import Result from './Result'
import '../styles/Results.css'

const Results = (props) => {
  const [selection, setSelection] = useState('all')
  const types = ['training', 'company', 'job']

  /**
   * Function to build an uuid if the result has no ID
   */
  const uuid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      const r = Math.random() * 16 | 0
      const v = c === 'x' ? r : (r & 0x3 | 0x8)
      return v.toString(16)
    })
  }

  /**
   * Computed functions
   */
  const getResults = () => {
    const results = { training: [], company: [], job: [] }
    if (props.loading) return results
    if (['all', 'training'].includes(selection)) results.training = props.apprenticeship.training
    if (['all', 'companies'].includes(selection)) results.company = props.apprenticeship.companies
    if (['all', 'jobs'].includes(selection)) results.job = props.apprenticeship.jobs
    return results
  }

  const totalResults = () => {
    const values = Object.values(getResults())
    return values.reduce((accumulator, value) => accumulator + value.length, 0)
  }

  return (
    <div className='column border p-0'>
      {/* Type of results */}
      <div className='tabs mb-0 px-4 pt-4'>
        <ul>
          <li className={`${selection === 'all' ? 'is-active' : ''}`}><a onClick={() => setSelection('all')}>Tous les résultats</a></li>
          <li className={`${selection === 'training' ? 'is-active' : ''}`}><a onClick={() => setSelection('training')}><IoSchoolSharp className='mr-2'/>Formations</a></li>
          <li className={`${selection === 'jobs' ? 'is-active' : ''}`}><a onClick={() => setSelection('jobs')}><IoBriefcaseSharp className='mr-2'/>Offres d&apos;emploi</a></li>
          <li className={`${selection === 'companies' ? 'is-active' : ''}`}><a onClick={() => setSelection('companies')}><IoHomeSharp className='mr-2'/>Entreprises</a></li>
        </ul>
      </div>

      {/* All the results */}
      <nav className='panel p-4'>
        {types.map(type => (
          getResults()[type].map(result => (
            <div className='panel-block' key={result.id || uuid()}>
              <Result type={type} result={result} />
            </div>
          ))
        ))}

        {props.loading &&
          <div className='panel-block subtitle ml-4'><div className='loader mr-4'></div>Chargement...</div>
        }

        {!props.loading && totalResults() === 0 &&
          <div className='panel-block subtitle ml-4'>Aucun résultat</div>
        }
      </nav>
    </div>
  )
}

Results.propTypes = {
  apprenticeship: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(Results)
