export const DIPLOMAS = {
  '3 (CAP...)': 'CAP, BEP',
  '4 (Bac...)': 'Baccalauréat',
  '5 (BTS, DUT...)': 'DEUG, BTS, DUT, DEUST',
  '6 (Licence...)': 'Licence, licence professionnel, maîtrise',
  '7 (Master, titre ingénieur...)': "Master, DEA, DESS, diplôme d'ingénieur"
}

export const CALLER = 'manon.blanco@hotmail.fr local_dev'
