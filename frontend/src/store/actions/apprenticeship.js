import { GET_JOBS_AND_TRAINING, GET_TRAINING, UPDATE_ERROR } from '../types'
import { CALLER } from '../../config'
import * as api from '../api'

export const getJobsAndTraining = (params) => async dispatch => {
  const urlParams = new URLSearchParams({ ...params, caller: CALLER })

  try {
    const data = await api.getJobsAndTraining(urlParams)
    const payload = {
      training: data.formations.results,
      jobs: data.jobs.peJobs.results,
      companies: data.jobs.lbaCompanies.results.concat(data.jobs.lbbCompanies.results)
    }
    dispatch({
      type: GET_JOBS_AND_TRAINING,
      payload
    })
  } catch (e) {
    dispatch({
      type: UPDATE_ERROR,
      payload: e
    })
  }
}

export const getTraining = (params) => async dispatch => {
  const urlParams = new URLSearchParams({ ...params, caller: CALLER })

  try {
    const data = await api.getTraining(urlParams)
    dispatch({
      type: GET_TRAINING,
      payload: data.results
    })
  } catch (e) {
    dispatch({
      type: UPDATE_ERROR,
      payload: e
    })
  }
}
