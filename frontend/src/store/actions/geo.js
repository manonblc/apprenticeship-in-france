import { GET_REGIONS, GET_DEPARTMENTS, GET_ADDRESSES, GET_ADDRESS, UPDATE_ERROR } from '../types'
import * as api from '../api'

const params = new URLSearchParams({ fields: ['nom', 'code'] })

export const getRegions = () => async dispatch => {
  try {
    const data = await api.getRegions(params)
    dispatch({
      type: GET_REGIONS,
      payload: data
    })
  } catch (e) {
    dispatch({
      type: UPDATE_ERROR,
      payload: e
    })
  }
}

export const getDepartments = () => async dispatch => {
  try {
    const data = await api.getDepartments(params)
    dispatch({
      type: GET_DEPARTMENTS,
      payload: data
    })
  } catch (e) {
    dispatch({
      type: UPDATE_ERROR,
      payload: e
    })
  }
}

export const getAddresses = (address) => async dispatch => {
  const params = new URLSearchParams({ q: address, limit: 10 })
  let payload = []

  if (address) {
    try {
      const data = await api.getAddresses(params)
      payload = data.features.reduce((accumulator, value) => {
        const ids = accumulator.map(a => a.id)
        const id = value.properties.id
        const label = value.properties.label
        if (!ids.includes(id)) accumulator.push({ id, label })
        return accumulator
      }, [])
    } catch (e) {
      dispatch({
        type: UPDATE_ERROR,
        payload: e
      })
      return
    }
  }

  dispatch({
    type: GET_ADDRESSES,
    payload
  })
}

export const getAddress = (address) => async dispatch => {
  const params = new URLSearchParams({ q: address, limit: 1 })
  try {
    const data = await api.getAddresses(params)
    if (!data.features.length) throw new Error('Addresse non trouvée')
    const value = data.features[0]
    const insee = value.properties.citycode
    const [longitude, latitude] = value.geometry.coordinates
    const payload = { insee, longitude, latitude }
    dispatch({
      type: GET_ADDRESS,
      payload
    })
  } catch (e) {
    dispatch({
      type: UPDATE_ERROR,
      payload: e
    })
  }
}
