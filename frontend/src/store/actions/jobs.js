import { GET_FAMILIES, GET_FIELDS, GET_PROFESSIONS, UPDATE_ERROR } from '../types'
import * as api from '../api'

export const getFamilies = () => async dispatch => {
  try {
    const data = await api.getFamilies()
    dispatch({
      type: GET_FAMILIES,
      payload: data
    })
  } catch (e) {
    dispatch({
      type: UPDATE_ERROR,
      payload: e
    })
  }
}

export const getFields = (family) => async dispatch => {
  try {
    const data = await api.getFields(family)
    dispatch({
      type: GET_FIELDS,
      payload: data
    })
  } catch (e) {
    dispatch({
      type: UPDATE_ERROR,
      payload: e
    })
  }
}

export const getProfessions = (family, field) => async dispatch => {
  try {
    const data = await api.getProfessions(family, field)
    dispatch({
      type: GET_PROFESSIONS,
      payload: data
    })
  } catch (e) {
    dispatch({
      type: UPDATE_ERROR,
      payload: e
    })
  }
}
