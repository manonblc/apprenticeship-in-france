import { BASE_URL, ADDRESS_URL, GEO_URL, APPRENTICESHIP_URL } from './urls'

export const getFamilies = async () => { return (await fetch(`${BASE_URL}/api/families`)).json() }
export const getFields = async (family) => { return (await fetch(`${BASE_URL}/api/${family}/fields`)).json() }
export const getProfessions = async (family, field) => { return (await fetch(`${BASE_URL}/api/${family}/${field}/professions`)).json() }

export const getAddresses = async (params) => { return (await fetch(`${ADDRESS_URL}/search?${params}`)).json() }

export const getRegions = async (params) => { return (await fetch(`${GEO_URL}/regions?${params}`)).json() }
export const getDepartments = async (params) => { return (await fetch(`${GEO_URL}/departements?${params}`)).json() }

export const getTraining = async (params) => { return (await fetch(`${APPRENTICESHIP_URL}/api/V1/formationsParRegion?${params}`)).json() }
export const getJobsAndTraining = async (params) => { return (await fetch(`${APPRENTICESHIP_URL}/api/V1/jobsEtFormations?${params}`)).json() }
