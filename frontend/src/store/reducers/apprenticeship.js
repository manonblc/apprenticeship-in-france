import { GET_JOBS_AND_TRAINING, GET_TRAINING } from '../types'

const initialState = {
  training: [],
  jobs: [],
  companies: []
}

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_JOBS_AND_TRAINING:
      return {
        ...state,
        ...action.payload
      }
    case GET_TRAINING:
      return {
        ...state,
        training: action.payload
      }
    default: return state
  }
}
