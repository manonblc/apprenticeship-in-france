import { GET_REGIONS, GET_DEPARTMENTS, GET_ADDRESSES, GET_ADDRESS } from '../types'

const initialState = {
  regions: [],
  departments: [],
  addresses: [],
  address: {}
}

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_REGIONS:
      return {
        ...state,
        regions: action.payload
      }
    case GET_DEPARTMENTS:
      return {
        ...state,
        departments: action.payload
      }
    case GET_ADDRESSES:
      return {
        ...state,
        addresses: action.payload
      }
    case GET_ADDRESS:
      return {
        ...state,
        address: action.payload
      }
    default: return state
  }
}
