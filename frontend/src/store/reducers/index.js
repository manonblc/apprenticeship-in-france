import { combineReducers } from 'redux'
import jobsReducer from './jobs'
import geoReducer from './geo'
import apprenticeshipReducer from './apprenticeship'
import utils from './utils'

export default combineReducers({
  jobs: jobsReducer,
  geo: geoReducer,
  apprenticeship: apprenticeshipReducer,
  utils: utils
})
