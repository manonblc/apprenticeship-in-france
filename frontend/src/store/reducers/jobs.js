import { GET_FAMILIES, GET_FIELDS, GET_PROFESSIONS } from '../types'

const initialState = {
  families: {},
  fields: {},
  professions: {}
}

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_FAMILIES:
      return {
        ...state,
        families: action.payload
      }
    case GET_FIELDS:
      return {
        ...state,
        fields: action.payload
      }
    case GET_PROFESSIONS:
      return {
        ...state,
        professions: action.payload
      }
    default: return state
  }
}
