import { UPDATE_ERROR } from '../types'

const initialState = {
  error: null
}

export default function (state = initialState, action) {
  switch (action.type) {
    case UPDATE_ERROR:
      return {
        ...state,
        error: action.payload.message
      }
    default: return state
  }
}
