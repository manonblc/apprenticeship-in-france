export const GET_FAMILIES = 'getFamilies'
export const GET_FIELDS = 'getFields'
export const GET_PROFESSIONS = 'getProfessions'

export const GET_ADDRESSES = 'getAddresses'
export const GET_ADDRESS = 'getAddress'

export const GET_REGIONS = 'getRegions'
export const GET_DEPARTMENTS = 'getDepartments'

export const GET_JOBS_AND_TRAINING = 'getJobsAndTraining'
export const GET_TRAINING = 'getTraining'

export const UPDATE_ERROR = 'updateError'
