export const BASE_URL = 'http://localhost:8080'

export const ADDRESS_URL = 'https://api-adresse.data.gouv.fr'

export const GEO_URL = 'https://geo.api.gouv.fr'

export const APPRENTICESHIP_URL = 'https://labonnealternance.apprentissage.beta.gouv.fr'
