import { bindActionCreators } from 'redux'
import * as actions from './store/actions'

export const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actions, dispatch)
}

export const mapStateToProps = (state) => {
  return {
    job: state.jobs,
    apprenticeship: state.apprenticeship,
    geo: state.geo,
    utils: state.utils
  }
}
